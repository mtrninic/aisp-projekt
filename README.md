Vrijeme trajanja funkcija: [

    Data(const std::string &datoteka) = 5050 ms

    search(const std::string& key) = 4799 ms

    deleteByKey(const std::string& key) = 4776 ms

    deleteNByValue(int column, const std::string& value) = 5039 ms

    std::string findMaxValue(int column) = 5204 ms

    void addRow(const std::vector<std::string>& newRow) = 4948 ms

    void addRows(const std::vector<std::vector<std::string>>& newRows) = 4760 ms
]


Data konstruktor prima argument koji je ima datoteke te čita cijelu datoteku, pri tome preskače prvi red jer su to nazivi stupaca.

Metoda search prima ključ i vraća sve vrijednosti u redu ako nađe ključ u klasi.

Metoda deleteByKey briše red ya napisani ključ kao argument.

Metoda deleteNByValue briše sve redove s određenom vrijednošću kao argument, također treba navesti koji stupac se gleda.

Metoda findMaxValue traži najveću vrijednost u nekom stupcu koji je naveden kao argument. S druge strane, findMinValue radi na istom principu
samo traži najmanju vrijednost u navedenom stupcu, ali nažalost ne radi trenutno.

Metoda addRow dodaje novi red u strukturu, addRows dodaje više redova u strukturu kao navedeni argumenti.


Koristio sam unordered_map kao strukturu podataka. Ključ mape je bio prvi stupac 'address' jer se radi o kriptovalutama pa pretpostavljam da 
treba ključ koji se ne može odmah dešifrirati. Vrijednost mape je bio vektor u kojem se spremaju stringovi i pomoću ključeva se mogu dohvatiti.
Zbog toga mislim da je to najsigurniji način da se spreme ovakvi tipovi podataka jer su jako sensitive (bar mi se tako čini jer se radi o 
kriptovalutama, a ljudi za to imaju wallete koji su također jako sigurni).
