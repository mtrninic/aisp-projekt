#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <unordered_map>
#include <map>
#include <string>
#include <limits>
#include <chrono>

class Data {
private:
    std::unordered_map<std::string, std::vector<std::string>> data;
public:
    Data(const std::string& datoteka) {
        std::ifstream file(datoteka);
        std::string line;
        bool firstRow = false;

        while (std::getline(file, line)) {
            if (!firstRow) {
                firstRow = true;
                continue; 
            }

            std::vector<std::string> row;
            std::stringstream ss(line);
            std::string value;

            while (std::getline(ss, value, ',')) {
                row.push_back(value);
            }

            if (!row.empty()) {
                std::string key = row[0];  
                row.erase(row.begin());    
                data[key] = row;           
            }
        }
    }

    void deleteByKey(const std::string& key) {
        data.erase(key);
    }

    void deleteNByValue(int column, const std::string& value) {
        for (auto it = data.begin(); it != data.end();) {
            std::vector<std::string>& row = it->second;
            if (column < row.size() && row[column] == value) {
                it = data.erase(it);
            } else {
                ++it;
            }
        }
    }

    std::vector<std::string> search(const std::string& key) const {
        auto it = data.find(key);
        if (it != data.end()) {
            return it->second;
        } else {
            return {};
        }
    }

    std::string findMaxValue(int column) const {
        if (data.empty()) {
            return "";
        }

        std::string maxValue = std::numeric_limits<std::string>::min();

        for (const auto& entry : data) {
    
            const std::vector<std::string>& row = entry.second;

            if (column < row.size()) {
                if (row[column] > maxValue) {
                    maxValue = row[column];
                }
            }
        }

        return maxValue;
    }

    std::string findMinValue(int column) const {
        if (data.empty()) {
            return ""; 
        }

        std::string minValue = std::numeric_limits<std::string>::max();

        for (const auto& entry : data) {

            const std::vector<std::string>& row = entry.second;
            
            if (column < row.size()) {
                if (row[column] <= minValue) {
                    minValue = row[column];
                }
            }
        }

        return minValue;
    }

    void addRow(const std::vector<std::string>& newRow) {
        if (newRow.empty()) {
            return; 
        }

        std::string key = newRow[0];  
        std::vector<std::string> values(newRow.begin() + 1, newRow.end());
        
        data[key] = values; 
    }

    void addRows(const std::vector<std::vector<std::string>>& newRows) {
        for (const auto& newRow : newRows) {
            addRow(newRow);
        }
    }

    void printData(int broj) const {
        int count = 0;
        for (const auto& entry : data) {
            if (count >= broj) {
                break;
            }
            std::cout << "Key: " << entry.first << ", Values: ";
            for (const auto& value : entry.second) {
                std::cout << "[" << value << "]" << " ";
            }
            std::cout << std::endl;
            count++;
        }
    }
};

int main() {
    Data data("BitcoinHeistData.csv");


    //Pretrazivanje po kljucu -----------------------------------------------------------------------------------------------
    std::string key = "132YLn4xBF1zPBnU6C38DyKCpEs8hRgie5";

    std::vector<std::string> rez = data.search(key);

    if (!rez.empty()) {
        std::cout << "Pronaden zapis s kljucem '" << key << "'." << std::endl;
    } else {
        std::cout << "Nije pronaden zapis s kljucem '" << key << "'." << std::endl;
    }

    //Brisanje s kljucem ----------------------------------------------------------------------------------------------------
    std::string key2 = "1HfJmDWAdBmViJwJMue4BwgDYVFRSp5np2";

    data.deleteByKey(key2);

    //Brisanje N zapisa pomocu vrijednosti ----------------------------------------------------------------------------------
    data.deleteNByValue(1, "2013");

    // Dohvacanje najvece vrijednosti ---------------------------------------------------------------------------------------
    std::string maxValue = data.findMaxValue(0);

    std::cout << "Najveca vrijednost u stupcu 'year' je: " << maxValue;
    
    // Dohvacanje najmanje vrijednosti --------------------------------------------------------------------------------------
    std::string minValue = data.findMinValue(0);

    std::cout << "Najmanja vrijednost u stupcu 'year' je: " << minValue;

    // Dodavanje jednog zapisa ----------------------------------------------------------------------------------------------
    std::vector<std::string> row = {"kljuc", "2011", "100", "54,1.53E-03", "5","1","4", "1.00E+09", "princetonCerber"};

    data.addRow(row);

    std::vector<std::string> rez2 = data.search("kljuc");

    /*
    if (!rez2.empty()) {
        std::cout << "Pronaden dodani zapis" << std::endl;
    } else {
        std::cout << "Nije pronaden dodani zapis"<< std::endl;
    }
    */
    
   // Dodavanje vise zapisa -------------------------------------------------------------------------------------------------
    std::vector<std::vector<std::string>> rows = {
        {"kljuc2", "2011", "100", "54,1.53E-03", "5","1","4", "1.00E+09", "princetonCerber"},
        {"kljuc3", "2011", "100", "54,1.53E-03", "5","1","4", "1.00E+09", "princetonCerber"}
    };

    data.addRows(rows);
    
    
    return 0;
}